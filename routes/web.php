<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//carreras
Route::get('/', 'carrerasController@home');//home
Route::get('/home', 'carrerasController@home');//home
Route::get('/races', 'carrerasController@listRaces');//Mostrar carreras
Route::get('/races/create', 'carrerasController@formCreate');//Crear carreras
Route::get('/races/create/check', 'carrerasController@check');//Comprovar carrera crada
Route::get('/races/edit/{id}', 'carrerasController@editRace');//editar carrera
Route::get('/races/edit/{id}/editCheck', 'carrerasController@editCheck');//Comprovar carrera editada
Route::get('/races/delete/{id}', 'carrerasController@deleteRace');//eliminar carrera
Route::get('/general-ranking', 'carrerasController@classGeneral');//Classificaio general
Route::get('/races/{id}/ranking', 'carrerasController@classRace'); //classificacio de la carrera de pilots



//pilots
Route::get('/races/{id}/driver/create', 'pilotController@createDriver');//crear pilot
Route::get('/races/{id}/driver/create/checkPilot', 'pilotController@checkPilot');//Comprovar pilot creat
Route::get('/races/{id_race}/driver/{id_driver}/edit', 'pilotController@editDriver');//editar pilot
Route::get('/races/{id_race}/driver/{id_driver}/edit/editCheckPilot', 'pilotController@editCheckPilot');//comprovar pilot editat

Route::get('/races/{id_race}/driver/{id_driver}/deleteDriver', 'pilotController@deleteDriver');//eliminar pilot

