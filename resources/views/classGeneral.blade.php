<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>MotoGP</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .taula{
                width:270px;
                margin-left:auto;
                margin-right:auto;
            }
            td{
                border: black 2px solid;
                border-collapse: separate;
            }
            table{
                border: black 2px solid;
                border-collapse: separate;
            }
            
        </style>
    </head>
    <body>
        <div class="content">
            <div class="title m-b-md">
                Clasificacion General MotoGP
            </div>
            <div class="taula">
                <table>
                    <tr>
                        <th>Posición</th>
                        <th>Piloto</th>
                        <th>Euipo</th>
                        <th>Puntos</th>
                    </tr>

                    <?php
                        foreach($arrayclasificacion as $element){
                    ?>
                    <tr>
                        <?php
                            foreach($element as $key => $value){
                        ?>  
                            <td>
                                <?php
                                    echo $value;}
                                ?>
                            </td>  
                    <?php
                    }
                    ?>
                    </tr>
                </table>
        </div>
    </body>
</html>
