<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Lista de Carreras</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .content {
                text-align: center;
                margin-top:40px;
            }

            .title {
                font-size: 84px;
            }
            .m-b-md {
                margin-bottom: 30px;
            }

            .llista{
                width:400px;
                margin-left:auto;
                margin-right:auto;
            }
            td{
                border: black 2px solid;
                border-collapse: separate;
            }
            table{
                border: black 2px solid;
                border-collapse: separate;
            }
        </style>
    </head>
    <body>
       

            <div class="content">
                <div class="title m-b-md">
                    Carreras MotoGP
                </div>                
            </div>
            <div class="llista">
                <table>
                    <tr>
                        <th>ID</th>
                        <th>Circuito</th>
                        <th>Fecha</th>
                        <th>Pais</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Valencia MotoGP</td>
                        <td>19 Novembre</td>
                        <td>SPAIN</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Jerez MotoGP</td>
                        <td>25 Novembre</td>
                        <td>SPAIN</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Sepang MotoGP</td>
                        <td>07 Febrer</td>
                        <td>MALAYSIA</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Qatar MotoGP</td>
                        <td>22 Febrer</td>
                        <td>QATAR</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Thailand</td>
                        <td>22 Març</td>
                        <td>SPAIN</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>Americas</td>
                        <td>05 Abril</td>
                        <td>EEUU</td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>Republica Argentina</td>
                        <td>19 Abril</td>
                        <td>ARGENTINA</td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>Jerez</td>
                        <td>03 Mayo</td>
                        <td>SPAIN</td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td>LEMANS</td>
                        <td>17 Mayo</td>
                        <td>FRANCE</td>
                    </tr>
                    <tr>
                        <td>10</td>
                        <td>Autodromo Internazionale del Mugello</td>
                        <td>31 Mayo</td>
                        <td>ITALY</td>
                    </tr>

                    <tr>
                        <td>11</td>
                        <td>Circuit de Catalunya</td>
                        <td>07 Juny</td>
                        <td>SPAIN</td>
                    </tr>
                    <tr>
                        <td>12</td>
                        <td>KymiRing</td>
                        <td>15 Mayo</td>
                        <td>FINLAND</td>
                    </tr>
                </table>
            </div>
        </div>
    </body>
</html>
