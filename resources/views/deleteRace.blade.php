<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                padding: 0px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            form{
                margin: 0 auto;
                align-content: center;
                border-radius: 10px;
                border: 1px solid #666666;
                width: 500px;
            }

            input[type="text"] {
                border-radius: 5px;
                margin: 10px;
                border: 1px solid #ccc;
                max-width: 100%;
                padding: 7px 8px;
            }
            input{
                text-align: center;
                color: #636b6f;
            }

            input[type="submit"] {
                border-radius: 5px;
                margin: 10px 0;
                background: #0088B2;
                color: #fff;
                padding: 8px 14px;
                font-weight: bold;
                border: none;
                width:100px;

            }

            .correcte{
                color:green;
            }

            .correcte{
                color:green;
            }
        </style>
    </head>
    <body>
    <div class="content">
                <div class="title m-b-md">
                    Eliminar Carrera de MotoGP
                </div>
                <h3>La carrera con la ID: <?php echo $id ?> se ha borrado correctamente </h3>
</body>
</html>