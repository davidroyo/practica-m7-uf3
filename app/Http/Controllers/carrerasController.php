<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class carrerasController extends Controller
{

    function home() {
        return view('home');
    }

    function listRaces() {
        return view('listRaces');
    }
    
    function formCreate () {
        return view('formCreate');
    }

    function check () {
        return view('check');
    }

    function editRace ($id) {
        return view('editRace',compact('id'));
    }

    function editCheck ($id) {
        return view('editCheck',compact('id'));
    }
    
    function deleteRace ($id) {
        return view('deleteRace',compact('id'));
    }
    
    function classGeneral () {
        
        $arrayclasificacion = ['pilot' =>   ['1','Guillem','Honda','40'],
                                            ['2','Uri','Ducati','39'],
                                            ['3','Ferran','Kawasaki','32'],
                                            ['4','Fore','Yamaha','29'],
                                            ['5','Alex','Aprilia','20'],
                                            ['6','Xavi','Yamaha','19'],
                                            ['7','David','Aprilia','15'],
                                            ['8','Uriol','Kawasaki','12'],
                                            ['9','Julia','Honda','10'],
                                            ['10','Fortes','Ducati','5'],
                                            ['11','Marc','Moster','2'],
                                ];
        return view('classGeneral',compact('arrayclasificacion'));
    }

    function classRace ($id) {

        $fecha = '22/11/2019';
        $carrera = 'Montmelo';
        $arrayclasificacion = ['pilot' =>   ['1','Guillem','Honda','40'],
                                            ['2','Uri','Ducati','39'],
                                            ['3','Ferran','Kawasaki','32'],
                                            ['4','Fore','Yamaha','29'],
                                            ['5','Alex','Aprilia','20'],
                                            ['6','Xavi','Yamaha','19'],
                                            ['7','David','Aprilia','15'],
                                            ['8','Uriol','Kawasaki','12'],
                                            ['9','Julia','Honda','10'],
                                            ['10','Fortes','Ducati','5'],
                                            ['11','Marc','Moster','2'],
                                ];

        return view('classRace', compact('id','fecha','carrera','arrayclasificacion'));
    }
}