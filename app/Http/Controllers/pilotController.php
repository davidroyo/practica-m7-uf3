<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class pilotController extends Controller
{
    function createDriver ($id) {

        return view('createDriver' ,compact('id'));
    }

    function checkPilot () {
        return view('checkPilot');
    }

    function editDriver ($id_race, $id_driver) {
        return view('editDriver', compact('id_race', 'id_driver'));
    }

    function editCheckPilot () {
        return view('editCheckPilot');
    }

    function deleteDriver ($id_driver,$id_race) {
        return view('deleteDriver', compact('id_race', 'id_driver'));
    }
}
